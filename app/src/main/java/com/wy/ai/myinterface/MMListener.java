package com.wy.ai.myinterface;

import com.huawei.hiai.vision.visionkit.face.Face;

import java.util.List;

public interface MMListener {
    void onTaskCompleted(List<Face> faces);
}