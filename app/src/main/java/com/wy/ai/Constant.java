package com.wy.ai;

public class Constant {

    //录音权限请求码
    public static final int REQUEST_PERMISSION_RECORD_AUDIO = 0x0001;

    //读写存储卡请求码
    public static final int REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE = 0x0002;

    //打开相机请求码
    public static final int REQUEST_PERMISSION_CAMERA = 0x0003;

    //拍照请求码
    public static final int REQUEST_IMAGE_TAKE = 100;
    //选择图片请求码
    public static final int REQUEST_IMAGE_SELECT = 200;

    //手机号码正则表达式
    public static final String REGEX_MOBILE = "^((17[0-9])|(14[0-9])|(13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";

}
