package com.wy.ai.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;


/**
 * @author wy
 * @deprecated 权限申请工具类
 */
public class PermissionUtils {

    private String permissions[] = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.SEND_SMS
    };
    private Activity activity;



    public PermissionUtils(Activity activity) {
        this.activity = activity;
    }

    /**
     * 检测一组权限
     */
    public void checkPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity, permissions[0])
                    != PackageManager.PERMISSION_GRANTED) {
                activity.requestPermissions(permissions, 100);
            }
        }
    }

    /**
     * 打开麦克风权限
     */
    public boolean requestRecordAudioPer() {
        if (ContextCompat.checkSelfPermission(activity, permissions[0]) !=
                PackageManager.PERMISSION_GRANTED) {
            showToast("请开启麦克风权限！");
            goSetting();
            return false;
        } else {
            return true;
        }
    }

    /**
     * 打卡相机权限
     */
    public boolean requestCameraPer() {
        if (ContextCompat.checkSelfPermission(activity, permissions[1]) !=
                PackageManager.PERMISSION_GRANTED) {
            showToast("请打开相机权限！");
            goSetting();
            return false;
        } else {
            return true;
        }
    }

    /**
     * 打开读写存储卡权限
     */
    public boolean requestWriteESPer() {
        if (ContextCompat.checkSelfPermission(activity, permissions[2]) !=
                PackageManager.PERMISSION_GRANTED) {
            showToast("请打开读写存储卡权限！");
            goSetting();
            return false;
        } else {
            return true;
        }
    }

    /**
     * 打开拨打电话权限
     */
    public boolean requestCallPhonePer() {
        if (ContextCompat.checkSelfPermission(activity, permissions[3]) !=
                PackageManager.PERMISSION_GRANTED) {
            showToast("请开启拨打电话权限！");
            goSetting();
            return false;
        } else {
            return true;
        }
    }

    /**
     * 打开发送短信权限
     */
    public boolean requstSendSmsPer() {
        if (ContextCompat.checkSelfPermission(activity, permissions[4]) !=
                PackageManager.PERMISSION_GRANTED) {
            showToast("请开启发送短信权限");
            goSetting();
            return false;
        } else {
            return true;
        }
    }

    private void showToast(String msg) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
    }

    private void goSetting() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.fromParts("package", activity.getPackageName(), null));
        activity.startActivity(intent);
    }
}
