package com.wy.ai.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CommonUitls {


    /**
     * 手机号码
     * @return
     */
    public static boolean isMobile(String num) {
        String regex = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(18[0,1,2,5-9])|(177))\\d{8}$";
        Pattern pattern=Pattern.compile(regex);
        Matcher matcher=pattern.matcher(num);
        return matcher.matches();
    }

    /**
     * 验证固话号码
     */
    public static boolean isTel(String num) {
        String regex = "^(0\\d{2}-\\d{8}(-\\d{1,4})?)|(0\\d{3}-\\d{7,8}(-\\d{1,4})?)$";
        Pattern pattern=Pattern.compile(regex);
        Matcher matcher=pattern.matcher(num);
        return matcher.matches();
    }


    /**
     * 是否是数字
     */
    public static  boolean isNum(String num){
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(num);
        if( !isNum.matches() ){
            return false;
        }
        return true;
    }
}
