package com.wy.ai.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.wy.ai.R;
import com.wy.ai.utils.CommonUitls;
import com.wy.ai.utils.PermissionUtils;


@RequiresApi(api = Build.VERSION_CODES.M)
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button bt_face_detector, bt_voice, bt_call, bt_msg;
    private PermissionUtils permissionUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bt_face_detector = findViewById(R.id.bt_face_detector);
        bt_voice = findViewById(R.id.bt_voice);
        bt_call = findViewById(R.id.bt_call);
        bt_msg = findViewById(R.id.bt_msg);

        bt_face_detector.setOnClickListener(this);
        bt_voice.setOnClickListener(this);
        bt_call.setOnClickListener(this);
        bt_msg.setOnClickListener(this);

        permissionUtils = new PermissionUtils(this);
        permissionUtils.checkPermission();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_face_detector:
                if (!permissionUtils.requestCameraPer()) {
                    return;
                } else if (!permissionUtils.requestWriteESPer()) {
                    return;
                } else {
                    goTargetActivity(FaceDetectorActivity.class);
                }
                break;
            case R.id.bt_voice:
                if (!permissionUtils.requestRecordAudioPer()) {
                    return;
                } else {
                    goTargetActivity(VoiceActivity.class);
                }
                break;
            case R.id.bt_call:
                if (!permissionUtils.requestCallPhonePer()) {
                    return;
                } else {
                    String num = "0551-67315790";
                    if (CommonUitls.isMobile(num)) {
                        callPhone(num);
                    } else if (CommonUitls.isTel(num)) {
                        callPhone(num);
                    } else {
                        Toast.makeText(this, "号码不正确，请检查号码", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.bt_msg:
                if (!permissionUtils.requstSendSmsPer()) {
                    return;
                } else {
                    String num = "18755175836";
                    if (CommonUitls.isMobile(num)) {
                        sendMessage(num);
                    } else if (CommonUitls.isTel(num)) {
                        sendMessage(num);
                    } else {
                        Toast.makeText(this, "号码不正确，请检查号码", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                break;
        }
    }

    private void sendMessage(String num) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        Uri uri = Uri.parse("smsto:" + num);
        intent.setData(uri);
        startActivity(intent);
    }

    private void callPhone(String num) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        Uri uri = Uri.parse("tel:" + num);
        intent.setData(uri);
        startActivity(intent);
    }


    private void goTargetActivity(Class cl) {
        Intent intent = new Intent(this, cl);
        startActivity(intent);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
